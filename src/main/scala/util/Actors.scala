package util

import java.util.concurrent.ExecutorService

import akka.japi.Creator
import akka.routing.RoundRobinRouter
import akka.routing._
import akka.actor.actorRef2Scala
import akka.pattern.gracefulStop
import akka.actor.ActorSystem
import akka.actor.Props
import scala.concurrent.Await
import scala.concurrent.duration._
import akka.pattern.ask
import akka.util.Timeout
import akka.actor._
import org.apache.hadoop.conf.Configuration

/**
 * Created by benjarman on 4/25/15.
 */

class MainEngineActor(conf: Configuration, numOfWorkers: Int=4) extends Actor with ActorLogging {

  val workerActors = context.actorOf(Props(new WorkerActor(conf)).withRouter(RoundRobinPool(numOfWorkers)), name = "WorkerActors")

  def receive: Receive = {
    case shutdown: Boolean => {
      val t = FiniteDuration(23L, "hours")

      Await.result(gracefulStop(workerActors, t, Broadcast(PoisonPill)), t)

    }
    case request => {
      workerActors forward request
    }
  }
}

class WorkerActor(conf: Configuration) extends Actor {

  val logger = new Logging(classOf[Actor].getCanonicalName)

  def receive: Receive = {
    case (
      dayPath: String,
      outBase: String,
      sDir: String,
      shouldNotProcess: String,
      flat: Boolean,
      createDir: Boolean
      ) => {
      try {
        val hdfsUtil = new Hdfs(conf)
        hdfsUtil.merge(dayPath, outBase, sDir, shouldNotProcess, flat, createDir)
      }catch{
        case e: Exception => {
          logger.error(e.getMessage)
        }

      }
    }
    case c => {
      val f = c.toString
      logger.error(s"Unsupported type of value: $f")
    }
  }
}

class Actors(conf: Configuration, numOfWorkers: Int=4) {

  implicit val timeout = Timeout(5 seconds)

  //val r = new MainEngineActor(conf, numOfWorkers)
  //val router = r.workerActors

  val _system = ActorSystem("MainEngineActor")
  val master = _system.actorOf(Props(new MainEngineActor(conf, numOfWorkers)), name = "EngineActor")

  def getEngine(): ActorRef ={
    return master
  }

  def shutdown(): Unit ={
    master ! true
    _system.shutdown()
  }

}
