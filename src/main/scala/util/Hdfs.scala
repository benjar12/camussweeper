package util

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileStatus, FileSystem, Path, FileUtil}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Created by benjarman on 4/2/15.
 */
class Hdfs(conf: Configuration) {

  val logger = new Logging("Hdfs")

  //get fs
  val fs = FileSystem.newInstance(conf)

  def exists(path: String): Boolean = {
    val p = new Path(path)
    fs.exists(p)
  }

  def isDir(path: String): Boolean = {
    val p = new Path(path)
    fs.isDirectory(p)
  }

  def createDir(path: String){
    logger.info(s"Creating dir:$path")
    val p = new Path(path)
    fs.mkdirs(p)
  }

  def delete(path: String){
    logger.info(s"Removing files in: $path")
    val p = new Path(path)
    fs.delete(p, true)
  }

  def ifNotExistsCreateDir(path: String){
    //validate out dir exists if not create
    if ( exists(path) )
      logger.info(s"$path Exists")
    else {
      logger.info(s"$path Does not Exist, creating")
      createDir(path)
    }
  }

  def getArrayOfFolders(path: String): Array[String] ={

    val p = new Path(path)

    def processFiles(fsin: Array[FileStatus]): Array[String] ={
      val re = Array[String]()
      if(fsin.isEmpty)
        re
      else{
        val h = fsin.head
        if (h.isDirectory){
          (re :+ fsin.head.getPath.toString) ++ processFiles(fsin.tail)
        }else{
          processFiles(fsin.tail)
        }
      }


    }
    processFiles( fs.listStatus(p) )

  }

  def getArrayOfFiles(path: String): Array[String] ={

    val p = new Path(path)

    def processFiles(fsin: Array[FileStatus]): Array[String] ={
      val re = Array[String]()
      if(fsin.isEmpty)
        re
      else{
        val h = fsin.head
        if (!h.isDirectory){
          (re :+ fsin.head.getPath.toString) ++ processFiles(fsin.tail)
        }else{
          processFiles(fsin.tail)
        }
      }


    }
    processFiles( fs.listStatus(p) )

  }

  def getArrayOfBoth(path: String): Array[String] ={

    val p = new Path(path)

    def processFiles(fsin: Array[FileStatus]): Array[String] ={
      val re = Array[String]()
      if(fsin.isEmpty)
        re
      else{
        val h = fsin.head
        (re :+ fsin.head.getPath.toString) ++ processFiles(fsin.tail)
      }


    }
    processFiles( fs.listStatus(p) )

  }

  def getRecursiveArrayOfFiles(path: String): Array[String] = {
    getArrayOfFiles(path) ++ getArrayOfFolders(path).foldLeft( Array[String]() )(  (r, p) => {r ++ getRecursiveArrayOfFiles(p) } )
  }

  def toPathArray(arr: Array[String]): Array[Path]={
    arr.foldLeft(Array[Path]())( (r, s) => {r ++ Array[Path](new Path(s))})
  }

  def getFinalFile(
                    fullPath: String,
                    sourceDir: String,
                    destDir: String,
                    flat: Boolean=true,
                    trimHours: Boolean=true,
                    addExt: Boolean=true): String ={

    val ext = if( conf.get("out.ext") == null ) ".deflate" else conf.get("out.ext")

    var outPath = ""

    //turn /year/month/day into year-month-day-hours
    //this is really bad and should re-write it
    if(flat) {
      //get the date string
      val splitVal = fullPath.split(s"$sourceDir/")(1).split(conf.get("out.ext"))(0)
      val startDate = if(trimHours) splitVal.dropRight(3) else splitVal

      logger.info(s"Attemting to create path: $startDate Before Trim: $splitVal")

      val newFileDate = DateTimeFormat.forPattern( conf.get("out.flatdateformat") ).print(
        DateTime.parse(startDate, DateTimeFormat.forPattern(conf.get("in.dateformat"))) )

      outPath = fullPath.replace(sourceDir, destDir).replace(splitVal, newFileDate)
    }else{
      outPath = fullPath.replace(sourceDir, destDir)
    }

    //remove hours
    //if(trimHours) outPath = outPath.dropRight(3)

    if(addExt) outPath += ext

    //return out path
    outPath

  }


  def merge(dayPath: String, outBase: String, sDir: String, shouldNotProcess: String, flat: Boolean=false, createDir: Boolean=false){

    val outPath = getFinalFile(dayPath, sDir, outBase, flat, false)

    //check if its to early to process that day
    if(dayPath.contains(shouldNotProcess)){
      logger.info(s"Skipping merge on: $dayPath")
    }else{
      logger.info(s"now merging files in: $dayPath")
      if(createDir) {

        val dir = getFinalFile(dayPath, sDir, outBase, flat, false, false).dropRight(3)
        //create directory in dest dir
        ifNotExistsCreateDir(dir)

      }

      val files = toPathArray( getRecursiveArrayOfFiles(dayPath) )

      //preform copyMerge
      FileUtilExt.copyMerge( fs, files, fs, new Path(outPath), false, conf, null )
    }

  }

}
