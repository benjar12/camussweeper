package util

import java.util.concurrent.ExecutorService

/**
 * Created by benjarman on 4/24/15.
 */
class RunnableSweeper(
                      threadPool: ExecutorService,
                       hdfsUtil: Hdfs,
                       dayPath: String,
                       outBase: String,
                       sDir: String,
                       shouldNotProcess: String,
                       flat: Boolean=false,
                       createDir: Boolean=false,
                       failedCount: Int=0
                       )
  extends Runnable {

  val logger = new Logging("RunnableSweeper")

  def stop(){
    Thread.currentThread().interrupt()
  }

  def run(): Unit ={
    try{

      hdfsUtil.merge(dayPath, outBase, sDir, shouldNotProcess, flat, createDir)
      //stop()

    }catch{
      case e: Exception => {
        logger.error(e.getMessage)
        val file = hdfsUtil.getFinalFile(dayPath, sDir, outBase, true, false)
        //hdfsUtil.delete(file)
        if(failedCount > 4 && threadPool.isShutdown == false) {
          Thread.sleep(1000L)
          logger.info("Going to retry: " + file)
          threadPool.submit(new RunnableSweeper(threadPool,hdfsUtil, dayPath, outBase, sDir, shouldNotProcess, flat, createDir, failedCount + 1) )
        }
      }
    }
  }

}