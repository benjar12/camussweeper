package util

/**
 * Created by benjarman on 4/1/15.
 */
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Logging(name: String){

    private [this] val logger = LoggerFactory.getLogger(name)

    logger.info("New Logger Created")

    private def th(): Long = { Thread.currentThread().getId() }

    private[this] def buildMessage(msg: String): String = {
        val t = th()
        s"THREAD[$t] "+ msg
    }

    def debug(msg: String){logger.debug(buildMessage(msg))}
    def info(msg: String){logger.info(buildMessage(msg))}
    def warn(msg: String){logger.warn(buildMessage(msg))}
    def error(msg: String){logger.error(buildMessage(msg))}
}
