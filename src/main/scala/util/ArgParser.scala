package util

/**
 * Created by benjarman on 4/1/15.
 */
class ArgParser(args : Array[String]) {

  val logger = new Logging("ArgParser")

  val arglist = args.toList
  type OptionMap = Map[Symbol, Any]

  //use recursion to parse args
  private[this] def nextOption(map : OptionMap, list: List[String]) : OptionMap = {
    def isSwitch(s : String) = (s(0) == '-')
    list match {
      case Nil => map
      case "-p" :: value :: tail =>
        nextOption(map ++ Map('properties -> value.toString), tail)
      case option :: tail => logger.warn(s"Unknown arg: $option")
        nextOption(map, tail)
    }
  }
  val options = nextOption(Map(),arglist)
  logger.info(s"Input Args: $options")

  def get(arg: String): Option[Any] ={
    options.get(Symbol(arg))
  }

}
