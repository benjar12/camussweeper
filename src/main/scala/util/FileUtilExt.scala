package util

/**
 * Created by benjarman on 4/2/15.
 */

import java.io.{InputStream, OutputStream, IOException}

import org.apache.commons.compress.utils.IOUtils
import org.apache.hadoop.fs.{FileStatus, FileSystem, Path, FileUtil}
import org.apache.hadoop.conf._
import org.apache.hadoop.io.compress.{CompressionOutputStream, CompressionCodec, CompressionCodecFactory, DecompressorStream, DeflateCodec}

object FileUtilExt extends FileUtil {

  var logger = new Logging("FileUtilExt")

  def copyMerge(
                 srcFS: FileSystem,
                 srcDirs: Array[Path],
                 dstFS: FileSystem,
                 dstFile: Path,
                 deleteSource: Boolean,
                 conf: Configuration,
                 addString: String): Boolean = {


    val file = dstFile.toString
    logger.info(s"Creating:$file")

    val outCompression = conf.get("output.mapred.map.output.compression.codec")
    val inCompression = conf.get("mapred.map.output.compression.codec")
    val isCompressed = conf.get("mapred.compress.map.output")

    //val out = Compression.CompressStream( dstFS.create(dstFile) )
    val out = dstFS.create(dstFile)

    logger.info("OUTPUT: " + outCompression + " INPUT: " + inCompression)

    val outStream = if(isCompressed == "true" && outCompression != null) {
      val codecFactory = new CompressionCodecFactory(conf)
      val codec = codecFactory.getCodecByClassName(outCompression)
      codec.createOutputStream(out)
    }else{
      out
    }

    srcDirs.foreach(p => {

      logger.info("Currently Working on: " + p.toString)

      val in = if (isCompressed == "true" && inCompression != null) {
        val factory = new CompressionCodecFactory(conf)
        val codec = factory.getCodecByClassName(conf.get("mapred.map.output.compression.codec"))
        codec.createInputStream(srcFS.open(p))
      } else {
        srcFS.open(p)
      }

      IOUtils.copy(in, outStream)

      in.close

      if (addString != null) outStream.write(addString.getBytes("UTF-8"))

      if (deleteSource) {srcFS.delete(p, true)}

    })

    outStream.close
    out.close

    true
  }
}

