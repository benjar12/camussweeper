package util

/**
 * Created by benjarman on 4/2/15.
 */
import java.io._
import java.util.zip.{InflaterOutputStream, InflaterInputStream, GZIPInputStream, DeflaterInputStream, DeflaterOutputStream}
import org.apache.hadoop.io.compress.CompressionOutputStream

case class BufferedReaderIterator(reader: BufferedReader) extends Iterator[String] {
  override def hasNext() = reader.ready
  override def next() = reader.readLine()
}

object Compression {

  def DeCompressStream(stream: InputStream): BufferedReaderIterator ={

   new BufferedReaderIterator(
      new BufferedReader(
        new InputStreamReader(
          new DeflaterInputStream(stream)
        )
      )
    )
  }

  def BufferReader(stream: InputStream): BufferedReaderIterator ={

    new BufferedReaderIterator(
      new BufferedReader(
        new InputStreamReader(
          stream
        )
      )
    )
  }

  def CompressStream(stream: OutputStream): OutputStreamWriter ={
    new OutputStreamWriter(
      new DeflaterOutputStream(stream)
    )
  }


}
