/**
 * Created by benjarman on 4/6/15.
 */
object SweepAndClean {
  def main(args: Array[String]){
    Sweeper.main(args)
    Cleaner.main(args)
  }
}
