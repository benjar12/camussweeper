import java.io.FileInputStream
import java.lang.Exception
import java.util.Properties

import com.typesafe.config.ConfigFactory
import org.apache.hadoop.conf.Configuration
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import util.{Logging, Hdfs, TimeZoneFactory, ArgParser}

/**
 * Created by benjarman on 4/1/15.
 */
object Cleaner {

  val logger = new Logging("Cleaner")

  def main(args: Array[String]){
    //parse the args
    val argParser = new ArgParser(args)

    //load the properties file
    val prop = new Properties()
    val pfile = argParser.get("properties").getOrElse("resources/local.properties").toString
    prop.load(new FileInputStream(pfile))
    val config = ConfigFactory.parseProperties(prop)

    //get conf vars
    val timezone = if(config.hasPath("timezone")) config.getString("timezone") else "UTC"
    val dateformat = if(config.hasPath("in.dateformat")) config.getString("in.dateformat") else "yyyy/MM/dd"
    val flatDateFormat = if(config.hasPath("out.flatdateformat")) config.getString("out.flatdateformat") else "yyyy-MM-dd"
    val daysAgo = config.getInt("clean.retain.days")
    val rootPath = config.getString("dir.root")
    val sourceDir = config.getString("dir.source")
    val destDir = config.getString("dir.dest")
    val flat = config.getBoolean("out.flat")
    val ext = config.getString("out.ext")

    //get the date we use to see if less than
    val cutOffDate = new DateTime(TimeZoneFactory(timezone) ).minusDays(daysAgo)

    //create the configuration
    val conf = new Configuration()

    //add optional conf vars
    val avConfs = List(
      "fs.defaultFS",
      "mapred.compress.map.output",
      "mapred.output.compression.type",
      "mapred.map.output.compression.codec",
      "out.ext"
    )
    avConfs.foreach(c =>{
      if(config.hasPath(c)){
        val value = config.getString(c)
        logger.info(s"$c Set as: $value")
        conf.set(c, value)
      }
    })

    //create our util for working with hadoop
    val hdfsUtil = new Hdfs(conf)

    //get topics
    val topicPaths = hdfsUtil.getArrayOfFolders(rootPath)
    topicPaths.foreach(topic => {

      val hourlyPath = s"$topic/$sourceDir"
      val dailyPath = s"$topic/$destDir"

      logger.info(s"Processing files for: $topic")

      //get files that have been grouped
      val daily = hdfsUtil.getArrayOfBoth(dailyPath)

      //iterate
      daily.foreach(day => processDays(day))

      def processDays(day: String){

        logger.info(s"Evaluating path for cleanup: $day")

        try {
          //this handles the case of yyyy/MM-dd and yyyy/MM/dd
          //we will use recursion to build the full date string
          //from the file path
          if (hdfsUtil.isDir(day)) {
            hdfsUtil.getArrayOfFiles(day).foreach(processDays(_))
          } else {
            //parse the date from the file
            val sDate = day.split(s"$destDir/")(1).split(s"$ext")(0)
            val fileDate = DateTime.parse(sDate, DateTimeFormat.forPattern(flatDateFormat))

            //check if is before cutOffDate
            val isBefore = fileDate.isBefore(cutOffDate)

            //if true delete files from hourly folder
            if (isBefore) {
              logger.info(s"going to process: $day")
              //build path to delete
              val d = DateTimeFormat.forPattern(dateformat).print(fileDate)
              val toDel = s"$hourlyPath/$d"
              //delete
              hdfsUtil.delete(toDel)
            } else {
              logger.info(s"Not ready to process: $day")
            }
          }
        }catch{
          case e: Exception => logger.error(e.getMessage)
        }
      }


    })
  }
}
