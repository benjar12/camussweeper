/**
 * Created by benjarman on 4/1/15.
 */
import java.util.Properties
import java.io.FileInputStream
import akka.routing.Broadcast
import akka.actor.PoisonPill
import org.apache.hadoop.conf._
import org.apache.hadoop.fs._
import org.apache.hadoop.io.compress.{CompressionCodec, GzipCodec,DeflateCodec}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import com.typesafe.config.ConfigFactory
import util.{ArgParser, Logging, Hdfs, TimeZoneFactory, RunnableSweeper, Actors}
import java.util.concurrent.{TimeUnit, Executors}
import scala.concurrent._


object Sweeper{

  val logger = new Logging("Sweeper")

  def main(args: Array[String]){

    //parse the args
    val argParser = new ArgParser(args)

    //load the properties file
    val prop = new Properties()
    val pfile = argParser.get("properties").getOrElse("resources/local.properties").toString
    prop.load(new FileInputStream(pfile))
    val config = ConfigFactory.parseProperties(prop)

    /*
      pull out the properties we need and set defaults
      Sample Values:
       dir.root=hdfs://{MASTER_NODE}/user/root/{DATA_DIR}/
       dir.source=hourly
       dir.dest=daily
       fs.defaultFS=
     */
    val rootPath = config.getString("dir.root")
    val sourceDir = config.getString("dir.source")
    val destDir = config.getString("dir.dest")
    val tmpDir = if(config.hasPath("dir.tmp")) config.getString("dir.tmp") else "tmpfiles"
    val flat = config.getBoolean("out.flat")
    val timezone = if(config.hasPath("timezone")) config.getString("timezone") else "UTC"
    val dateformat = if(config.hasPath("in.dateformat")) config.getString("in.dateformat") else "yyyy/MM/dd"
    val outdateformat = if(config.hasPath("out.flatdateformat")) config.getString("out.flatdateformat") else "yyyy/MM/dd"
    val sweeperStartFrom = if(config.hasPath("sweep.start.date")) config.getString("sweep.start.date") else "1990/01/01"
    val ext = config.getString("out.ext")
    val numOfWorkers = if(config.hasPath("sweeper.workers")) config.getInt("sweeper.workers") else 2

    //I really dont like that this gets used in util.Hdfs it should be used in the clean function below
    val shouldNotProcess = DateTimeFormat.forPattern( dateformat ).print( new DateTime(TimeZoneFactory(timezone) ) )
    //Used in the clean functionality below. Will not process days before this date
    val sweeperStartFromDate =  DateTime.parse(sweeperStartFrom, DateTimeFormat.forPattern(dateformat))

    logger.info(s"Will skip: $shouldNotProcess and anything before: $sweeperStartFrom")

    logger.info("Starting to process files in %1$s" format(sourceDir))

    //create the configuration
    val conf = new Configuration()

    //add optional conf vars
    val avConfs = List(
      "fs.defaultFS",
      "mapred.compress.map.output",
      "mapred.output.compression.type",
      "mapred.map.output.compression.codec",
      "out.ext",
      "in.dateformat",
      "out.flatdateformat",
      "output.mapred.map.output.compression.codec"
    )
    avConfs.foreach(c =>{
      //set input conf
      if(config.hasPath(c)){
        val value = config.getString(c)
        logger.info(s"$c Set as: $value")
        conf.set(c, value)
      }

    })

    //create our thread pool
    val actorSystem = new Actors(conf, numOfWorkers)
    val engine = actorSystem.getEngine()

    //create our util for working with hadoop
    val hdfsUtil = new Hdfs(conf)

    //get topics
    val topicPaths = hdfsUtil.getArrayOfFolders(rootPath)
    logger.info("Found these topics for processing: " + topicPaths.reduceLeft(_+", "+_))

    def process_topic(path: String){

      //if not create dirs
      /*hdfsUtil.delete(s"$path/$tmpDir")
      hdfsUtil.ifNotExistsCreateDir(s"$path/$tmpDir")
      hdfsUtil.ifNotExistsCreateDir(s"$path/$destDir")*/

      val hours = ()=> {

        //get files from source dir
        val yearPaths = hdfsUtil.getArrayOfFolders(s"$path/$sourceDir")
        val monthPaths = yearPaths.foldLeft(Array[String]())((r, c) => r ++ hdfsUtil.getArrayOfFolders(c))
        val dayPaths = monthPaths.foldLeft(Array[String]())((r, c) => r ++ hdfsUtil.getArrayOfFolders(c))

        //Clean out days that have already been processed.
        // Because cleaner will also be running this should not take very long
        val cleanPaths = dayPaths.foldLeft(Array[String]())((r,c) =>{

          val hdfsUtil = new Hdfs(conf)

          //get the final file from the source folder and validate does not exist
          val outPath = hdfsUtil.getFinalFile(c, sourceDir, destDir, true, false)
          val inHdfs = hdfsUtil.exists(outPath)

          //extract date from file
          val sDate = outPath.split(s"$destDir/")(1).split(s"$ext")(0)
          val fileDate = DateTime.parse(sDate, DateTimeFormat.forPattern(outdateformat))

          //check is before allowed start date
          val isBeforeAllowD = fileDate.isBefore(sweeperStartFromDate)

          logger.info(s"$outPath Already Exists: $inHdfs Before date: $isBeforeAllowD")

          //if all checks out put it in cleanPaths and we will process it next
          if(!inHdfs && !isBeforeAllowD){
            r ++ Array(c.toString)
          }else{
            r
          }

        })

        //agg the hours into one dir
        //flat: Boolean=false, createDir: Boolean=true
        cleanPaths.foreach( c => {
          logger.info(s"$c is in the queue")
          //dayPath: String, outBase: String, sDir: String, shouldNotProcess: String, flat: Boolean=false, createDir: Boolean=true
          engine ! (c, destDir, sourceDir, shouldNotProcess, flat, false)

        } )

      }
      hours()

    }

    //process the topic
    topicPaths foreach(process_topic(_))

    actorSystem.shutdown()

  }

}
