#!/bin/bash
ROOTDIR=/locm
APPNAME=sweeper_cleaner
APPDIR=$ROOTDIR/$APPNAME
CLASSNAME=SweepAndClean
CONFIGFILE=$APPDIR/config/dev.properties
JAR=$APPDIR/sweeper_cleaner-assembly-1.0.jar
LOG=$APPDIR/logs/$CLASSNAME.log

export HADOOP_USER_NAME=hdfs

hadoop jar $JAR $CLASSNAME -p $CONFIGFILE >> $LOG >2&1