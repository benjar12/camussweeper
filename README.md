#Author
Ben Jarman < bjarman@local.com >

# Overview
This code base is meant to reduce the number of files by grouping days into one file
It basiclly takes hdfs://la3devmaster01/user/root/orwell/data/i/hourly/2015/03/03/*/* 
and puts all of those files in hdfs://la3devmaster01/user/root/orwell/data/i/daily/2015-03-03.deflate

#Requirements
sbt is required.

#Usage
1. sbt assembly
2. hadoop jar target/scala-2.11/sweeper_cleaner-assembly-1.0.jar Sweeper -p src/main/resources/remote.properties
3. hadoop jar target/scala-2.11/sweeper_cleaner-assembly-1.0.jar Cleaner -p src/main/resources/remote.properties

#Notes
Cleaner not yet impl..
